#!/bin/sh
set -e
export HOME=`pwd`

TARGETS=${TARGETS:-"linux/amd64 linux/arm linux/arm64 linux/mipsle linux/mips64le windows/386 windows/amd64 windows/arm64"}
BUILD_LDFLAGS=${BUILD_LDFLAGS:-"-s -w -buildid="}
BINARY_DIR=$HOME/binary
MARCHAPP_DIR=$HOME/marchbin
RAY_BUILD_VER=${RAY_BUILD_VER:-"master"}
export CGO_ENABLED=${CGO_ENABLED:-'0'}
AMD64_MARCHES="v1 v2 v3 v4"
ARM_MARCHES="6 7"

function goBuild() {
  #GOOS=windows GOARCH=amd64 GOAMD64=v2 goBuild output_fimename source_path
  BUILD_SRCFILENAME=${2:-'.'}
  BUILD_BINFILEEXTNAME=''
  GOARCH=${GOARCH:-'amd64'}
  GOOS=${GOOS:-'linux'}
  BUILD_CPUARCHNAME=$GOARCH
  if [ "$GOARCH" = "amd64" ]; then
    GOAMD64=${GOAMD64:-'v1'}
    BUILD_CPUARCHNAME='x86_64-'$GOAMD64
  fi
  if [ "$GOARCH" = "arm" ]; then
    GOARM=${GOARM:-'6'}
    BUILD_CPUARCHNAME='armv'$GOARM
  fi
  if [ "$GOARCH" = "386" ]; then
    BUILD_CPUARCHNAME='x86'
  fi
  if [ "$GOOS" = "windows" ]; then
    BUILD_BINFILEEXTNAME='.exe'
  fi
  echo "build ${1##*/} for $GOOS on $BUILD_CPUARCHNAME"
  go build -trimpath -ldflags="$BUILD_LDFLAGS" -o $1-$GOOS-$BUILD_CPUARCHNAME$BUILD_BINFILEEXTNAME $BUILD_SRCFILENAME
}

echo "Building marchbin"
mkdir $MARCHAPP_DIR
cd gosrc
mkdir temp
for ma in $AMD64_MARCHES; do
  GOARCH=amd64 GOAMD64=$ma goBuild temp/archTEST simpleapp.go
done
cd temp
tar --numeric-owner -c -z -f $MARCHAPP_DIR/x86_64.tar.gz *
cd ..
cd ..

mkdir -p $BINARY_DIR
echo 'Fetching sources files...'
git clone https://github.com/v2fly/v2ray-core.git v2ray-core
cd v2ray-core
git checkout $RAY_BUILD_VER

go get -t -d ./...

sed -i '/switch size {/,/defaultBufferSize = -1/s/case 0:/case -1:/' features/policy/policy.go

cd main

for t in $TARGETS; do
  echo "Building binary for target $t"
  if [ "${t%/*}" = "windows" ]; then
    nameext='.exe'
  else
    nameext=''
  fi
  if [ "${t#*/}" = "amd64" ]; then
    for ma in $AMD64_MARCHES; do
      GOOS=${t%/*} GOARCH=amd64 GOAMD64=$ma goBuild $BINARY_DIR/v2ray
    done
  elif [ "${t#*/}" = "arm" ]; then
    for ma in $ARM_MARCHES; do
      GOOS=${t%/*} GOARCH=arm GOARM=$ma goBuild $BINARY_DIR/v2ray
    done
  else
    GOOS=${t%/*} GOARCH=${t#*/} goBuild $BINARY_DIR/v2ray
  fi
done

gzip -n -9 $BINARY_DIR/*

cd $HOME

echo "Downloading geosite and geoip data..."
curl -Lo assets/geosite.dat https://github.com/v2fly/domain-list-community/releases/latest/download/dlc.dat
curl -Lo assets/geoip.dat https://github.com/v2fly/geoip/releases/latest/download/geoip.dat
curl -Lo assets/geoip-only-cn-private.dat https://github.com/v2fly/geoip/releases/latest/download/geoip-only-cn-private.dat
curl -Lo assets/cn.dat https://github.com/v2fly/geoip/releases/latest/download/cn.dat
curl -Lo assets/private.dat https://github.com/v2fly/geoip/releases/latest/download/private.dat

gzip -n -9 assets/*.dat

sed -i 's|DOWNLOAD_BASE_URL=|\0"'$CI_JOB_URL/artifacts/raw'"|' $HOME/scripts/install.sh

#echo 'building geoip and domain-list-community...'
#go get -u -insecure github.com/v2ray/geoip
#go get -u -insecure github.com/v2ray/domain-list-community

#echo 'Fetching GeoLite2 data...'
#curl -L 'https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country-CSV&license_key=JvbzLLx7qBZT&suffix=zip' -o GeoLite2-Country-CSV.zip
#unzip -q GeoLite2-Country-CSV.zip
#rm -f GeoLite2-Country-CSV.zip
#mv GeoLite2* GeoLite2-Country-CSV

#cd $HOME/assets
#echo 'Generating geoip and geosite file...'
#geoip --country=../GeoLite2-Country-CSV/GeoLite2-Country-Locations-en.csv --ipv4=../GeoLite2-Country-CSV/GeoLite2-Country-Blocks-IPv4.csv --ipv6=../GeoLite2-Country-CSV/GeoLite2-Country-Blocks-IPv6.csv
#domain-list-community
#mv dlc.dat geosite.dat

cd $BINARY_DIR
if [ "$(ls -A .)" ]; then
  echo 'Build finished, waiting for artifacts storing process...'
  exit 0
fi

echo 'No build found at release directory!'
exit 1
