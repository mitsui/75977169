#!/bin/sh
systemctl disable v2ray || true
systemctl stop v2ray || true
rm -f /usr/local/bin/v2ray
rm -f /etc/systemd/system/v2ray.service
rm -rf /etc/v2ray
