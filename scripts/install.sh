#!/bin/bash

set -e

DOWNLOAD_BASE_URL=

check_kernel_version(){
  kernel_ver=(`uname -r | grep -o -e ^[0-9]*.[0-9]*.[0-9]* | tr . "\n"`)
  test_ver=(`echo $1 | tr . "\n"`)
  for order in 0 1 2; do
    if [ "${test_ver[$order]}" = "" ]; then
      return 0
    fi;
    if [ ${kernel_ver[$order]} -lt ${test_ver[$order]} ]; then
      return 1
    fi
    if [ ${kernel_ver[$order]} -gt ${test_ver[$order]} ]; then
      return 0
    fi
  done
  return 0
}

input_boolean() {
  while true; do
    read -n 1 -p "$1 " user_choose
    if [ "$user_choose" = "" ]; then
      [ "$2" = "" ] || user_choose=$2
    else
      echo >/dev/tty
    fi;
    case "${user_choose^^}" in
      Y)
        echo 1
        return 0
        ;;
      N)
        echo 0
        return 1
        ;;
    esac
    echo -e "\e[31mUnexcepted input, try again.\e[0m" >/dev/tty
  done
}

fetchFile() {
  if type curl > /dev/null; then
    DOWNLOADER='curl -L'
  elif type wget > /dev/null; then
    DOWNLOADER='wget -O-'
  else
    echo -e "\e[31mCan not download file: no download tool\e[0m" >&2
    exit 1
  fi
  if [ "x$2" = "x" ]; then
    $DOWNLOADER $1
  else
    $DOWNLOADER $1 | gzip -c -d - >$2
  fi
}

cd `dirname $(readlink -f $0)`
test -e /etc/v2ray/config.d || mkdir -p /etc/v2ray/config.d
arch=`uname -m`
case "$arch" in
    i?86) arch=386 ;;
    x86_64) arch=x86_64-v1 ;;
    armv6l) arch=armv6 ;;
    armv7l) arch=armv7 ;;
    armv8l) arch=arm64 ;;
esac
work_dir=/tmp/$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c16)
mkdir -p $work_dir
cd $work_dir

if [ "$arch" = "x86_64-v1" ]; then
  fetchFile $DOWNLOAD_BASE_URL/marchbin/x86_64.tar.gz | tar xz
  ./archTEST-linux-x86_64-v2 > /dev/null 2>&1 && arch=x86_64-v2
  ./archTEST-linux-x86_64-v3 > /dev/null 2>&1 && arch=x86_64-v3
  ./archTEST-linux-x86_64-v4 > /dev/null 2>&1 && arch=x86_64-v4
fi
echo -e "\e[36mCurrent platform architecture is $arch\e[0m"

echo -e '\e[36mDownloading v2ray binary...\e[0m'
fetchFile "$DOWNLOAD_BASE_URL/binary/v2ray-linux-$arch.gz" ./v2ray
install ./v2ray /usr/local/bin
install_service=yes
install_geodat=yes
service_file=/etc/systemd/system/v2ray.service

if [ -e /etc/v2ray/geoip.dat ] && [ -e /etc/v2ray/geosite.dat ]; then
  install_geodat=no
  if input_boolean 'Install(update) geoip and geosite file? (Y/n): ' y >/dev/null; then
    install_geodat=yes
  fi
fi

if [ -e $service_file ]; then
  if ! input_boolean 'Update/Rewrite exist service file? (Y/n): ' y >/dev/null; then
    install_service=no
  fi
fi

if [ "$install_service" = "yes" ]; then
  fetchFile $DOWNLOAD_BASE_URL/assets/v2ray.service >v2ray.service
  cp -f v2ray.service /etc/systemd/system
  chmod 0644 $service_file
  systemctl daemon-reload || true
fi

if [ ! -e /etc/v2ray/config.json ]; then
  echo -e "\e[32mYou may need creating your configs\e[0m"
fi

if [ ! -e /etc/v2ray/env ]; then
  echo '# Environment variables for v2ray' >/etc/v2ray/env
  echo 'V2RAY_BUF_READV=enable' >>/etc/v2ray/env
  echo '# V2RAY_CONF_GEOLOADER=standard' >>/etc/v2ray/env
  echo 'V2RAY_LOCATION_ASSET=/etc/v2ray' >>/etc/v2ray/env
  echo 'V2RAY_LOCATION_CONFIG=/etc/v2ray' >>/etc/v2ray/env
  echo 'V2RAY_LOCATION_CONFDIR=/etc/v2ray/config.d' >>/etc/v2ray/env
  echo '# V2RAY_RAY_BUFFER_SIZE=2' >>/etc/v2ray/env
fi

if [ "$install_geodat"="yes" ]; then
  echo -e "\e[36mDownloading and installing geoip.dat and geosite.dat\e[0m"
  fetchFile $DOWNLOAD_BASE_URL/assets/geoip.dat.gz geoip.dat
  fetchFile $DOWNLOAD_BASE_URL/assets/geosite.dat.gz geosite.dat
  fetchFile $DOWNLOAD_BASE_URL/assets/cn.dat.gz cn.dat
  fetchFile $DOWNLOAD_BASE_URL/assets/private.dat.gz private.dat
  cp -f geoip.dat /etc/v2ray
  cp -f geosite.dat /etc/v2ray
  cp -f cn.dat /etc/v2ray
  cp -f private.dat /etc/v2ray
fi

if [ ! -e /etc/v2ray/cert.pem ]; then
  echo -e "\e[36mGenerating a self-signed certificate for v2ray\e[0m"
  openssl ecparam -genkey -name prime256v1 -out /etc/v2ray/key.pem
  openssl req -key /etc/v2ray/key.pem -x509 -days 1826 -subj "/C=US/ST=Utah/L=Orem/O=V2/OU=Org/CN=*" -out /etc/v2ray/cert.pem
  chmod 0644 /etc/v2ray/key.pem
fi

rm -rf $work_dir

echo -e "\e[32mUse 'systemctl start v2ray' to start service.\e[0m"
